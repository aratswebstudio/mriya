<?php

class SearchController extends \BaseController
{

    public function search()
    {
        $data = Input::all();

        $validator = Validator::make($data, Search::rules(), Search::errors());

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        } else {

            $q = DB::table('objects');
            $q->where('active', 1);

            if (!empty($data['id'])) {
                $q->where('id', $data['id']);
            } else {
                $dat_exactly = ['location_id', 'case_type', 'type_id', 'currency', 'area'];
                $dat_min_max = ['price', 'area', 'rooms'];

                foreach ($dat_exactly as $value) {
                    if (!empty($data[$value])) {
                        $q->where($value, $data[$value]);
                    }
                }

                foreach ($dat_min_max as $value2) {
                    if (!empty($data['min_' . $value2])) {
                        $q->where($value2, '>=', $data['min_' . $value2]);
                    }

                    if (!empty($data['max_' . $value2])) {
                        $q->where($value2, '<=', $data['max_' . $value2]);
                    }
                }
            }

        }
        $objects = $q->get();

        return View::make('pages.search', ['objects' => $objects]);
    }

}
