<?php

class Location extends \Eloquent {
    protected $fillable = ['name'];

    public static function rules() {
        return $relues = [
            'name' => 'required|min:3|max:255'
        ];
    }

    public static function errors() {
        return $errors = [
            'name.required' => 'Введіть назву',
            'name.min' => 'Назва має бути від :min символів',
            'name.max' => 'Назва має бути до :max символів'
        ];
    }

    public static function locationEdit($data, $id) {
        $item = Location::find($id);

        $item->name = $data['name'];
        $item->save();

        return $item;
    }
}
