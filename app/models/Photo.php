<?php

class Photo extends \Eloquent {
    protected $fillable = ['category_id'];

    public function photos() {
        return $this->hasMany('Photo');
    }

    public static function rules() {
        return $rules = [
            'photo1' => 'image',
            'photo2' => 'image',
            'photo3' => 'image',
            'photo4' => 'image',
            'photo5' => 'image',
        ];
    }

    public static function errors() {
        return $errors = [
            'photo1.image' => 'авантажений файл має бути формату jpeg, png, bmp або gif',
            'photo2.image' => 'авантажений файл має бути формату jpeg, png, bmp або gif',
            'photo3.image' => 'авантажений файл має бути формату jpeg, png, bmp або gif',
            'photo4.image' => 'авантажений файл має бути формату jpeg, png, bmp або gif',
            'photo5.image' => 'авантажений файл має бути формату jpeg, png, bmp або gif',

        ];

    }

    public function property() {
        return $this->belongsTo('Object');
    }


    public static function photoUpload($cover, $id) {

        $coverDir = public_path().'/images/catalog/'.$id.'/';
        $coverDir2 = '/images/catalog/'.$id.'/';

        if (!is_dir($coverDir)) {
            mkdir($coverDir,0700);
            chmod($coverDir, 0777);
        }

        $coverName = $id.'_'.time().'.jpg';

        $cover->move($coverDir, $coverName);

        $coverToResize = $coverDir.$coverName;

        Image::make($coverToResize)->resize(850, null, function ($constraint) {$constraint->aspectRatio();})->save($coverToResize);


        DB::table('photos')->insert(array('object_id' => $id, 'path' => $coverDir2.$coverName));
        DB::table('photos')->where('object_id', 0)->delete();
        chmod($coverToResize, 0777);
    }
}
