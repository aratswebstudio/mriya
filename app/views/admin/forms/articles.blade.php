<div class="form-group">
	<label class="col-sm-2 col-sm-2 control-label">Заголовок</label>
	<div class="col-lg-10">
		@if(isset($item))
			{{ Form::input('text', 'name', $item->name, ['class' => 'form-control', 'required']) }}
		@else
			{{ Form::input('text', 'name', '', ['class' => 'form-control' , 'required']) }}
		@endif
			@if($errors->has('name'))
				@foreach($errors->get('name') as $nameError)
					<div class="alert alert-block alert-danger fade in">
		              <button data-dismiss="alert" class="close close-sm" type="button">
		                <i class="icon-remove"></i>
		              </button>
		              <strong>Помилка!</strong> {{$nameError}}
		            </div>
            	@endforeach
           @endif
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 col-sm-2 control-label">Ключові слова (SEO)</label>
	<div class="col-lg-10">
		@if(isset($item))
			{{ Form::input('text', 'meta_k', $item->meta_k, ['class' => 'form-control']) }}
		@else
			{{ Form::input('text', 'meta_k', '', ['class' => 'form-control']) }}
		@endif
			@if($errors->has('meta_k'))
				@foreach($errors->get('meta_k') as $meta_kError)
					<div class="alert alert-block alert-danger fade in">
		              <button data-dismiss="alert" class="close close-sm" type="button">
		                <i class="icon-remove"></i>
		              </button>
		              <strong>Помилка!</strong> {{$meta_kError}}
		            </div>
            	@endforeach
           @endif
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label col-sm-2">Ключове описання (SEO)</label>
	<div class="col-sm-10">
			@if(isset($item))
				{{ Form::textarea('meta_d', $item->meta_d, ['class' => 'form-control', 'rows' => '3'])}}
			@else
				{{ Form::textarea('meta_d', '', ['class' => 'form-control', 'rows' => '3'])}}
			@endif

			@if($errors->has('meta_d'))
				@foreach($errors->get('meta_d') as $meta_dError)
					<div class="alert alert-block alert-danger fade in">
				      <button data-dismiss="alert" class="close close-sm" type="button">
				        <i class="icon-remove"></i>
				      </button>
				      <strong>Помилка!</strong> {{$meta_dError}}
				    </div>
				@endforeach
    		@endif
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label col-sm-2">Описання</label>
	<div class="col-sm-10">
			@if(isset($item))
				{{ Form::textarea('body', $item->body, ['class' => 'form-control ckeditor', 'rows' => '6'])}}
			@else
				{{ Form::textarea('body', '', ['class' => 'form-control ckeditor', 'rows' => '6'])}}
			@endif

			@if($errors->has('body'))
				@foreach($errors->get('body') as $bodyError)
					<div class="alert alert-block alert-danger fade in">
				      <button data-dismiss="alert" class="close close-sm" type="button">
				        <i class="icon-remove"></i>
				      </button>
				      <strong>Ошибка!</strong> {{$bodyError}}
				    </div>
				@endforeach
    		@endif
	</div>
</div>

<div class="form-group">
	<label class="control-label col-md-2">Обкладинка</label>
	<div class="col-md-10">
		<div class="fileupload fileupload-new" data-provides="fileupload">
				<div class="fileupload-new thumbnail" style="max-width: 400px; max-height: 300px; line-height: 20px;">
					<img src="@if(isset($item)){{ $item->cover }} @endif" alt="">
				</div>
			<div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 400px; max-height: 300px;"></div>
			<div>
				<span class="btn btn-white btn-file">
					<span class="fileupload-new"><i class="icon-paper-clip"></i> Змінити фото</span>
					<span class="fileupload-exists"><i class="icon-undo"></i> Інше</span>
					<input type="file" name="cover" class="default">
				</span>
			</div>
		</div>
		@if($errors->has('cover'))
			@foreach($errors->get('cover') as $coverError)
				<div class="alert alert-block alert-danger fade in">
	              <button data-dismiss="alert" class="close close-sm" type="button">
	                <i class="icon-remove"></i>
	              </button>
	              <strong>Помилка!</strong> {{$coverError}}
	            </div>
	    	@endforeach
    	@endif
	</div>
</div>

@if(isset($item))
<div class="form-group">
	<label class="control-label col-md-2">Активність</label>
		<div class="col-sm-10">
                <div class="switch switch-square" data-on-label="<i class=' icon-ok'></i>" data-off-label="<i class='icon-remove'></i>">
                    <input type="checkbox" name="active" value="1" @if(isset($item->active) && $item->active == 1) checked @endif />
               </div>
		</div>
</div>
@endif

@if(isset($item)) <input type="hidden" name="id" value="{{ $item->id }}"> @endif

<div class="form-group">
	<label class="col-sm-2 control-label"></label>
	<div class="col-sm-10 pull-left">
		<button type="submit" class="btn btn-info "><i class="icon-refresh"></i> @if(isset($item))Зберегти @else Додати @endif</button>
	</div>
</div>