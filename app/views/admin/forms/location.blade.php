<div class="form-group">
    <label class="col-sm-2 col-sm-2 control-label">Назва локації</label>
    <div class="col-lg-10">
        @if(isset($item))
            {{ Form::input('text', 'name', $item->name, ['class' => 'form-control', 'required']) }}
        @else
            {{ Form::input('text', 'name', '', ['class' => 'form-control' , 'required']) }}
        @endif
        @if($errors->has('name'))
            @foreach($errors->get('name') as $nameError)
                <div class="alert alert-block alert-danger fade in">
                    <button data-dismiss="alert" class="close close-sm" type="button">
                        <i class="icon-remove"></i>
                    </button>
                    <strong>Помилка!</strong> {{$nameError}}
                </div>
            @endforeach
        @endif
    </div>
</div>

@if(isset($item)) <input type="hidden" name="id" value="{{ $item->id }}"> @endif

<div class="form-group">
    <label class="col-sm-2 control-label"></label>
    <div class="col-sm-10 pull-left">
        <button type="submit" class="btn btn-info "><i class="icon-refresh"></i> @if(isset($item))Зберегти @else Додати @endif</button>
    </div>
</div>
