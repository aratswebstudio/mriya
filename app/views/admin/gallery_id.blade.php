@extends('admin.layouts.layout')

@section('title')
    @if(isset($project->title)){{ $project->title }} | @endif Галерея
@stop

@section('styles')
    <!-- Bootstrap core CSS -->
    <link href="/admin/css/bootstrap.min.css" rel="stylesheet"/>
    <link href="/admin/css/bootstrap-reset.css" rel="stylesheet"/>
    <!--external css-->
    <link href="/admin/assets/font-awesome/css/font-awesome.css" rel="stylesheet"/>

    <link rel="stylesheet" type="text/css" href="/admin/assets/bootstrap-datepicker/css/datepicker.css"/>
    <link rel="stylesheet" type="text/css" href="/admin/assets/bootstrap-daterangepicker/daterangepicker.css"/>
    <link rel="stylesheet" type="text/css" href="/admin/assets/bootstrap-fileupload/bootstrap-fileupload.css"/>
    <link rel="stylesheet" type="text/css" href="/admin/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css"/>

    <!-- Custom styles for this template -->
    <link href="/admin/css/style.css" rel="stylesheet"/>
    <link href="/admin/css/style-responsive.css" rel="stylesheet"/>
    <link href="/admin/assets/fancybox/source/jquery.fancybox.css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="/admin/css/gallery.css"/>
    <link href="/admin/assets/dropzone/css/dropzone.css" rel="stylesheet"/>
@stop

@section('content')
    <!-- page start-->
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                    @if(isset($item->name)) {{ $item->name }} - Галерея фото @endif
                </header>
                @if($errors->any())
                    <div class="alert alert-block alert-danger fade in">
                        <button data-dismiss="alert" class="close close-sm" type="button">
                            <i class="icon-remove"></i>
                        </button>
                        <strong>Ошибка!</strong> {{$errors->first()}}
                    </div>
                @endif
                <div class="panel-body">
                    <ul class="grid cs-style-3">
                        @foreach($item->photos as $photo)
                            <li>
                                <figure>
                                    <img src="{{ $photo->path }}" alt="img04">
                                    <figcaption>
                                        <h3>{{ $photo->path }}</h3>
                                        {{--<span>lorem ipsume </span>--}}
                                        <a class="fancybox" rel="group" href="/dashboard/gallery/drop/{{ $photo->id }}">Видалити</a>
                                    </figcaption>
                                </figure>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </section>

            <!--main content start-->
            <section id="main-content">
                <section class="wrapper site-min-height">
                    <!-- page start-->
                    <section class="panel">
                        <header class="panel-heading">
                            Завантажте фото
                        </header>
                        <div class="panel-body">
                            {{-- <form action="assets/dropzone/upload.php" class="dropzone" id="my-awesome-dropzone"> --}}
                            {{ Form::open(['method' => 'post', 'enctype' => 'multipart/form-data', 'class' => 'dropzone222', 'id' => 'my-awesome-dropzone222']) }}
                            <input type="hidden" name="id" value="{{ $item->id }}">
                            <div class="form-group">
                                <input type="file" name="photo1" class="form-control">
                            </div>
                            {{--<div class="form-group">--}}
                                {{--<input type="file" name="photo2" class="form-control">--}}
                            {{--</div>--}}
                            {{--<div class="form-group">--}}
                                {{--<input type="file" name="photo3" class="form-control">--}}
                            {{--</div>--}}
                            {{--<div class="form-group">--}}
                                {{--<input type="file" name="photo4" class="form-control">--}}
                            {{--</div>--}}
                            {{--<div class="form-group">--}}
                                {{--<input type="file" name="photo5" class="form-control">--}}
                            {{--</div>--}}
                            {{-- ПРОСТУЮ ФОРМУ НА 5 ИНПУТОВ --}}
                            <div class="form-group">
                                <button type="submit" class="btn btn-info">Завантажити</button>
                            </div>
                            {{ Form::close() }}
                        </div>
                    </section>
                    <!-- page end-->
                </section>
            </section>
            <!--main content end-->
        </div>
    </div>
    <!-- page end-->
@stop

@section('scripts')
    <!-- js placed at the end of the document so the pages load faster -->
    <script src="/admin/js/jquery.js"></script>
    <script src="/admin/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="/admin/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="/admin/assets/fancybox/source/jquery.fancybox.js"></script>
    <script src="/admin/js/jquery.scrollTo.min.js"></script>
    <script src="/admin/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="/admin/js/respond.min.js"></script>
    <script src="/admin/assets/dropzone/dropzone.js"></script>

    <script src="/admin/js/modernizr.custom.js"></script>
    <script src="/admin/js/toucheffects.js"></script>


    <!--common script for all pages-->
    <script src="/admin/js/common-scripts.js"></script>

    <script type="text/javascript">
        $(function () {
            //    fancybox
            jQuery(".fancybox").fancybox();
        });

    </script>
@stop