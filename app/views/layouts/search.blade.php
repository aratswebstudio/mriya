<section>
    <div class="section-content">
        <div class="container">
            <div class="section-header onscroll-animate" data-animation="fadeInLeft">
                <h1>Пошук по каталогу</h1>
                <h4>Скористайтесь нашою системою пошуку об'єктів нерухомості, щоб знайти пропозицію саме для Вас!</h4>
            </div>
            {{-- WARNING --}}
            {{-- <div class="alert alert-info">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <strong>Увага!</strong> У зв'язку із профілактикою, форма пошуку тимчасово не працює. Просимо вибачити за тимчасові незручності.
            </div> --}}
            {{-- END WARNING --}}
            <form id="form-search" action="/search" method="post">
                <div class="row">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-sm-6">
                                <p>ID пропозиції</p>
                                <input type="text" placeholder="Номер..." name="id">
                            </div>
                            <div class="col-sm-6">
                                <p>Місцезнаходження</p>
                                <div class="custom-select">
                                    <div class="custom-select-val"></div>
                                    <ul class="custom-select-list">
                                        <li class="custom-select-default disabled" data-val="0">
                                            <div class="custom-select-item-content">Будь-яке...</div>
                                        </li>
                                        @foreach($locations as $location)
                                            <li data-val="{{ $location->id }}" onChange="myFunction()">
                                                <div class="custom-select-item-content">{{ $location->name }}</div>
                                            </li>
                                        @endforeach
                                    </ul>
                                    <input type="hidden" id="location_id" name="location_id">
                                </div>
                            </div>
                        </div><!-- .row -->
                    </div><!-- .col-md-6 -->
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-sm-6">
                                <p>Тип пропозиції</p>
                                <div class="custom-select">
                                    <div class="custom-select-val"></div>
                                    <ul class="custom-select-list">
                                        <li class="custom-select-default" data-val="0">
                                            <div class="custom-select-item-content">Будь-яка...</div>
                                        </li>
                                        <li data-val="оренда">
                                            <div class="custom-select-item-content">Оренда</div>
                                        </li>
                                        <li data-val="продаж">
                                            <div class="custom-select-item-content">Продаж</div>
                                        </li>
                                        <li data-val="обмін">
                                            <div class="custom-select-item-content">Обмін</div>
                                        </li>
                                    </ul>
                                    <input type="hidden" id="case_type" name="case_type">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <p>Вид об'єкта</p>
                                <div class="custom-select">
                                    <div class="custom-select-val"></div>
                                    <ul class="custom-select-list">
                                        <li class="custom-select-default" data-val="0">
                                            <div class="custom-select-item-content">Будь-який...</div>
                                        </li>
                                        @foreach($object_types as $type)
                                            <li data-val="{{ $type->id }}">
                                                <div class="custom-select-item-content">{{ $type->name }}</div>
                                            </li>
                                        @endforeach
                                    </ul>
                                    <input type="hidden" id="type_id" name="type_id">
                                </div>
                            </div>
                        </div><!-- .row -->
                    </div><!-- .col-md-6 -->
                </div><!-- .row -->

                <div class="row">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-sm-4">
                                <p>Мінімальна ціна</p>
                                <input type="text" placeholder="..." name="min_price">
                            </div>
                            <div class="col-sm-4">
                                <p>Максимальна ціна</p>
                                <input type="text" placeholder="..." name="max_price">
                            </div>
                            <div class="col-sm-4">
                                <p>Валюта</p>
                                <div class="custom-select">
                                    <div class="custom-select-val"></div>
                                    <ul class="custom-select-list">
                                        <li class="custom-select-default" data-val="0">
                                            <div class="custom-select-item-content">Будь-яка</div>
                                        </li>
                                        <li class="custom-select-default" data-val="грн.">
                                            <div class="custom-select-item-content">грн.</div>
                                        </li>
                                        <li class="custom-select-default" data-val="$">
                                            <div class="custom-select-item-content">$</div>
                                        </li>
                                    </ul>
                                    <input type="hidden" id="currency" name="currency">
                                </div>
                            </div>
                        </div><!-- .row -->
                    </div><!-- .col-md-6 -->

                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-sm-4">
                                <p id="min_area">Мінімальна площа</p>
                                <input type="text" placeholder="..." name="min_area">
                            </div>
                            <div class="col-sm-4">
                                <p id="max_area">Максимальна площа</p>
                                <input type="text" placeholder="..." name="max_area">
                            </div>
                            <div class="col-sm-4">
                                <p>Тип площі</p>
                                <div class="custom-select">
                                    <div class="custom-select-val"></div>
                                    <ul class="custom-select-list">
                                        <li class="custom-select-default" data-val="0">
                                            <div class="custom-select-item-content">Будь-який</div>
                                        </li>
                                        <li class="custom-select-default" data-val="м.кв.">
                                            <div class="custom-select-item-content">кв.м.</div>
                                        </li>
                                        <li class="custom-select-default" data-val="сотки">
                                            <div class="custom-select-item-content">сотки</div>
                                        </li>
                                        <li class="custom-select-default" data-val="гектари">
                                            <div class="custom-select-item-content">гектари</div>
                                        </li>
                                    </ul>
                                    <input type="hidden" id="area" name="area_type">
                                </div>
                            </div>
                        </div>
                    </div><!-- .col-md-6 -->
                </div><!-- .row -->

                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-sm-6">
                                <p>Кімнат від</p>
                                <input type="text" placeholder="..." name="min_rooms">
                            </div>
                            <div class="col-sm-6">
                                <p>Кімнат до</p>
                                <input type="text" placeholder="..." name="max_rooms">
                            </div>
                        </div><!-- .row -->
                    </div><!-- .col-md-6 -->

                </div><!-- .row -->
                <p class="text-center onscroll-animate" data-animation="flipInY">
                    <button type="submit" id="form-search-submit" class="button-void button-long">знайти <i class="fa fa-search"></i></button>
                </p>
            </form>
        </div><!-- .container -->
    </div><!-- .section-content -->
</section>
