@extends('layouts.layout')

@section('meta_k')  @stop
@section('meta_d')  @stop
@section('title') Пошук об'єктів @stop

@section('content')
    <section>
        <div class="section-content">
            <div class="container">
                <div class="section-header onscroll-animate" data-animation="fadeInLeft" id="list">
                    <h1>Каталог нерухомості</h1>
                    <h4>Для вибору пропозиції саме для вас, скористайтесь пошуком</h4>
                </div>
                <div class="row">
                    @if(isset($objects) && count($objects))
                        @foreach ($objects as $item)
                            {{--<div class="col-md-4 onscroll-animate">--}}
                                <article class="col-md-4 onscroll-animate">
                                    <div class="post-preview preview-item">
                                        <a href="/catalog/item/{{ $item->id }}">
                                            <section>
                                                <div class="post-preview-img">
                                                    <div class="post-preview-img-inner">
                                                        <img alt="post img" src="{{ $item->cover }}">
                                                    </div>
                                                    <div @if($item->case_type == 'оренда') class="post-preview-label"
                                                         @else class="post-preview-label2" @endif>{{ $item->case_type }}</div>
                                                    @if($item->termins == 1)
                                                        <div class="post-preview-label-alt-wrapper">
                                                            <div class="post-preview-label-alt">
                                                                Терміново {{-- <i class="fa fa-star"></i> --}}</div>
                                                        </div>
                                                    @endif
                                                </div>
                                            </section>
                                        </a>
                                        <div class="post-preview-content">
                                            <h2 class="post-preview-heading">{{ $item->name }}</h2>
                                            {{--<p>{{ str_limit($item->description, 70, '...') }}</p>--}}
                                            <div class="post-preview-price-container">
                                                <a href="/catalog/item/{{ $item->id }}" class="read-more-link-alt">Детальніше</a>
                                                <p class="listing-price">
                                                    @if($item->agreement_price == true)
                                                        Договірна
                                                    @else
                                                        @if($item->currency !== "грн."){{ $item->currency }} {{ $item->price }}  @else {{ $item->price }} {{ $item->currency }} @endif
                                                        <span class="small">{{ $item->pay_type }}</span>
                                                    @endif
                                                </p>
                                            </div>
                                            <div class="post-preview-detail">
                                                {{ $item->area }} {{ $item->area_type }}
                                                @if($item->rooms !== 0 && $item->rooms !== "" &&  $item->rooms !== NULL )
                                                    <span class="delimiter-inline-alt"></span>
                                                    {{ $item->rooms }} кімнат(и)@endif

                                                @if($item->remote !== 0 && $item->remote !== "" &&  $item->remote !== NULL )
                                                    <span class="delimiter-inline-alt"></span>
                                                    {{ $item->remote }} км. від Києва@endif
                                            </div>
                                        </div>
                                    </div>
                                </article>
                            {{--</div><!-- .col-md-4 -->--}}
                        @endforeach
                    @else
                        <div class="alert alert-info">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <strong>Інформація!</strong> Нажаль, за заданими Вами параметрами нічого не знайдено.
                            Спробуйте уточнити параметри пошуку або перевірте введені дані.
                        </div>
                    @endif
                </div><!-- .row -->
            </div><!-- .container -->
        </div><!-- .section-content -->
    </section>
    @include('layouts.search')
@stop
